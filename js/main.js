var target_block;

$(document).ready(function(){
	$(".server__title").click(function(){
		if(!$(this).parents(".server").hasClass("server_soon")){
			if($(this).siblings(".server__content").is(":visible")){
				$(this).siblings(".server__content").slideUp();
			}
			else{
				$(".server__title").siblings(".server__content").slideUp();
				$(this).siblings(".server__content").slideDown();
			}	
		}
	});

	var features_slider = $('.project-carousel');

	features_slider.owlCarousel({
	    loop:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        500:{
	            items:2,
	            margin: 20
	        },
	        768:{
	            items:3,
	            margin: 20
	        },
	        1000:{
	            items:3,
	            margin: 20
	        }
	    }
	});

	$('.project-carousel-arrow__next').click(function() {
	    features_slider.trigger('next.owl.carousel');
	})

	$('.project-carousel-arrow__prev').click(function() {
	    features_slider.trigger('prev.owl.carousel');
	})

	$(".donate-item").hover(function(){
		if($(this).parent().hasClass("donate-items")){
			$(this).children(".donate-item__description").fadeIn(300);
		}
	},
	function(){
		$(this).children(".donate-item__description").fadeOut(300);
	});
	
	$(".header-links__a").click(function(){
		target_block = $(this).data("section");
	    $([document.documentElement, document.body]).animate({
	        scrollTop: $("."+target_block).offset().top
	    }, 1000);
	    return false;
	});
	
	$(".header-btn").click(function(){
	    $([document.documentElement, document.body]).animate({
	        scrollTop: $(".information").offset().top
	    }, 1000);
	    return false;
	});
});